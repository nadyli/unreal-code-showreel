#include "SurveillanceCamera.h"

// Sets default values
ASurveillanceCamera::ASurveillanceCamera() {
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	lerpAlpha = 0.0f;
	holdTime = 0.0f;
	holdRemaining = 0.0f;
	reverseDirection = false;
	rotationSpeed = 1.0f;
}

// Called when the game starts or when spawned
void ASurveillanceCamera::BeginPlay() {
	Super::BeginPlay();
}

// Called every frame
void ASurveillanceCamera::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	// Lukitaan lerpAlpha-muuttujan arvo
	lerpAlpha = FMath::Clamp(lerpAlpha, 0.0f, 1.0f);

	// Jos on aktiivinen, teemme seuraavat:
		// Jos hold timer on enemm�n kuin 0 (aktiivisesti p��ll�), laskemme ajastinta alasp�in
		// Kun lerpAlpha on 1 tai 0, asetamme holdTimerin ja k��nnymme ymp�ri
	if(state) {
		if(holdRemaining > 0.0f) {
			holdRemaining -= 1 * DeltaTime;
		} else {
			if(lerpAlpha == 0.0 || lerpAlpha == 1.0) {
				// K��nnet��n kameran suunta ymp�ri ja asetetaan ajastimelle aika
				reverseDirection = !reverseDirection;
				holdRemaining = holdTime;
			}

			// Muutetaan lerpAlpha-arvoa haluttuun suuntaan
			if(reverseDirection) lerpAlpha -= rotationSpeed * DeltaTime;
			else lerpAlpha += rotationSpeed * DeltaTime;
			// Lasketaan uusi kiertokulma
			FRotator newRot = FMath::Lerp(startPosition, endPosition, lerpAlpha);
			// K��nnet��n objektia
			SetActorRotation(newRot);
		}
	}
}