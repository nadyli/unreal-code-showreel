#pragma once

#include "CoreMinimal.h"
#include "AsteroidFieldSaveData.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "AsteroidField.generated.h"


UCLASS()
class ESCAPEINSPACE_API AAsteroidField : public AActor {
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AAsteroidField();
	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "AsteroidField")
		UBoxComponent* triggerBox;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AsteroidField")
		int maxAsteroidsInField;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AsteroidField")
		TArray<TSubclassOf<AActor>> asteroidTypes;

	// WeakPtr
	//TArray<TWeakPtr<AActor>> asteroids;
	TArray<TWeakObjectPtr<AActor>> asteroids;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AsteroidField")
		FVector initialAsteroidMinSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AsteroidField")
		FVector initialAsteroidMaxSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AsteroidField")
		FVector initialAsteroidMinTorque;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AsteroidField")
		FVector initialAsteroidMaxTorque;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	float torqueFactor = 250;
	UFUNCTION()
		void DestroyObject(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);


public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintCallable, Category = "AsteroidField")
		void DestroyAllAsteroids();
	UFUNCTION(BlueprintCallable, Category = "AsteroidField")
		void SpawnAsteroid(FAsteroidData a);
};
