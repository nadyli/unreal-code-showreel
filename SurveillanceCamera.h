// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "CoreMinimal.h"
#include "Toggleable.h"
#include "SurveillanceCamera.generated.h"

UCLASS()
class ESCAPEINSPACE_API ASurveillanceCamera : public AToggleable {
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASurveillanceCamera();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SurveillanceCamera")
		FRotator startPosition;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SurveillanceCamera")
		FRotator endPosition;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SurveillanceCamera")
		float rotationSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SurveillanceCamera")
		float holdTime;
	float lerpAlpha;
	float holdRemaining;
	bool reverseDirection;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void Disable();
	void Enable();

	void OnRaiseAlarm();
};
