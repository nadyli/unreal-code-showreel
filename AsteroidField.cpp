#include "AsteroidField.h"
#include "AsteroidBase.h"
#include "BigAsteroid.h"
#include "EscapeInSpace.h"
#include "Containers/Set.h"

// Sets default values
AAsteroidField::AAsteroidField() {
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	triggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("RootComponent"));
	// Tuhoutuminen
	triggerBox->SetGenerateOverlapEvents(true);
	triggerBox->OnComponentEndOverlap.AddDynamic(this, &AAsteroidField::DestroyObject);

	RootComponent = triggerBox;
}

// Called when the game starts or when spawned
void AAsteroidField::BeginPlay() {
	Super::BeginPlay();

	int asteroidTypesCount = asteroidTypes.Num();
	if(asteroidTypesCount < 1) {
		UE_LOG(EscapeInSpace, Warning, TEXT("No asteroid elements added to set in AsteroidField."));
		return;
	}

	for(int i = 0; i < maxAsteroidsInField; i++) {
		int randomIDForAsteroidToSpawn = FMath::RandRange(0, asteroidTypesCount - 1);
		TSubclassOf<AActor> randomAsteroid = asteroidTypes[randomIDForAsteroidToSpawn];
		if(randomAsteroid.Get()->IsValidLowLevel() == false) {
			UE_LOG(EscapeInSpace, Warning, TEXT("Asteroid element %d is not valid in %s."), randomIDForAsteroidToSpawn, *GetName());
			continue;
		}

		FVector boxExtentsHalf = triggerBox->GetScaledBoxExtent() * 0.5;
		FVector spawnLocation = this->GetActorLocation() + FVector(FMath::RandRange(-boxExtentsHalf.X, boxExtentsHalf.X), FMath::RandRange(-boxExtentsHalf.Y, boxExtentsHalf.Y), FMath::RandRange(-boxExtentsHalf.Z, boxExtentsHalf.Z));
		FRotator spawnRotation = FRotator::ZeroRotator;
		FActorSpawnParameters spawnParams;
		spawnParams.Owner = this;

		TWeakObjectPtr<AActor> newAsteroid = GetWorld()->SpawnActor<AActor>(randomAsteroid, spawnLocation, spawnRotation, spawnParams);
		newAsteroid->Tags.Add(FName("Asteroid"));

		// Lis�t��n nopeus, suunta ja v��nt�momentti
		if(newAsteroid.IsValid()) {
			TArray<UStaticMeshComponent*> meshComponent;
			newAsteroid->GetComponents(meshComponent, false);
			if(meshComponent.Num() > 0) {
				meshComponent[0]->SetPhysicsLinearVelocity(FVector(FMath::RandRange(initialAsteroidMinSpeed.X, initialAsteroidMaxSpeed.X), FMath::RandRange(initialAsteroidMinSpeed.Y, initialAsteroidMaxSpeed.Y), FMath::RandRange(initialAsteroidMinSpeed.Z, initialAsteroidMaxSpeed.Z)));
				meshComponent[0]->AddTorqueInDegrees(FVector(FMath::RandRange(initialAsteroidMinTorque.X, initialAsteroidMaxTorque.X), FMath::RandRange(initialAsteroidMinTorque.Y, initialAsteroidMaxTorque.Y), FMath::RandRange(initialAsteroidMinTorque.Z, initialAsteroidMaxTorque.Z)) * torqueFactor, NAME_None, true);
			}
		}

		asteroids.Add(newAsteroid);

		/*
		// WeakPtr esimerkki
		// SpawnActor palauttaa raa'an osoittimen
		AActor * newAsteroid = GetWorld()->SpawnActor<AActor>(type, spawnLocation, spawnRotation, spawnParams);
		// Tehd��n jaettu viittaus raakaan osoittimeen (roskienhallinta hoitaa t�t�)
		TSharedRef<AActor> temp(newAsteroid);
		// Tehd��n heikko osoitin jaetusta osoittimesta
		TWeakPtr<AActor> tempWeak(temp);

		// Varmistetaan objektin olemassaolo ennen k�ytt��
		if(TSharedPtr<AActor> shared = tempWeak.Pin()) {
			// K�sittely shared-osoittimen kautta
			if(shared.IsValid() == false) return;
			// Lis�t��n taulukkoon
			asteroids.Add(shared);
		}
		*/
	}
}

// Called every frame
void AAsteroidField::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	TArray<TWeakObjectPtr<AActor>> toBeRemoved;

	for(auto& t : asteroids) {
		if(t.IsValid() == false) toBeRemoved.Add(t);
	}

	while(toBeRemoved.Num()) {
		asteroids.Remove(toBeRemoved[0]);
		toBeRemoved.RemoveAt(0);
	}
}

void AAsteroidField::DestroyObject(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {
	if(OtherActor->Tags.Contains("Asteroid")) {
		OtherActor->Destroy();
	}
}

void AAsteroidField::DestroyAllAsteroids() {
	UE_LOG(EscapeInSpace, Log, TEXT("Destroyed all asteroids"));
	for(auto& a : asteroids) {
		if(a.IsValid()) a->Destroy();
	}
}

void AAsteroidField::SpawnAsteroid(FAsteroidData a) {
	FVector spawnLocation = a.position;
	FRotator spawnRotation = a.rotation;
	FActorSpawnParameters spawnParams;
	spawnParams.Owner = this;

	TWeakObjectPtr<AActor> newAsteroid;

	switch(a.type) {
		case EAsteroidType::Regular:
			newAsteroid = GetWorld()->SpawnActor<AActor>(AAsteroidBase::StaticClass(), spawnLocation, spawnRotation, spawnParams);
			break;
		case EAsteroidType::Big:
			newAsteroid = GetWorld()->SpawnActor<AActor>(ABigAsteroid::StaticClass(), spawnLocation, spawnRotation, spawnParams);
			break;
	}

	if(newAsteroid.IsValid() == false) {
		UE_LOG(EscapeInSpace, Error, TEXT("Failed to create an asteroid from save data."));
		return;
	}

	newAsteroid->Tags.Add(FName("Asteroid"));
	newAsteroid->SetActorScale3D(a.scale);

	TArray<UStaticMeshComponent*> meshComponent;
	newAsteroid->GetComponents(meshComponent, false);
	if(meshComponent.Num() > 0) {
		meshComponent[0]->SetPhysicsLinearVelocity(a.speed);
		meshComponent[0]->SetPhysicsAngularVelocity(a.angularVelocity);
	}

	asteroids.Add(newAsteroid);
}